# Covong Covid Delivery
Covong Covid Delivery เป็นเว็บสำหรับแสดงข้อมูลสถิติของโรคระบาด COVID-19 และผู้ใช้สามารถสมัครรับข้อมูลผ่านอีเมล โดยข้อมูลจะถูกส่งผ่านอีเมลเป็นรายวัน ซึ่งเวลาไม่แน่นอนขึ้นอยู่กับ API ของ https://covid19.ddc.moph.go.th/ <br/><br/>
![home page](screenshots/home.png) 

## การติดตั้ง
### ซอฟแวร์ที่จำเป็น
- [BellSoft Liberica JDK version 11](https://bell-sw.com/pages/downloads/#/java-11-lts)
- [Maven](https://maven.apache.org/download.cgi)
- [MariaDB](https://mariadb.org/download/) หรือ [MySQL](https://www.mysql.com/downloads/)
### ฐานข้อมูล
1. สร้างผู้ใช้ฐานข้อมูล
2. สร้างฐานข้อมูลที่ผู้ใช้มีสิทธิ์เต็ม(เพิ่ม, ลบ, แก้ไข)
### อีเมล
1. ลงทะเบียนอีเมลของ gmail<br/>
2. สร้างรหัสผ่านสำหรับแอปตามวิธีที่ [google แนะนำ](https://support.google.com/accounts/answer/185833) โดยเลือกแอปตอนสร้างรหัสเป็น **อีเมล**<br/>
<img src="screenshots/password-for-app.png" alt="password for app" width="500" /><br/>
### ตั้งค่าความปลอดภัย
1. คัดลอกไฟล์ `src/main/resources/application.yaml.example` ไปเป็น `src/main/resources/application.yaml` (ตัดส่วน .example ออก)<br/>
2. เปิดไฟล์ข้างต้นและตั้งค่าดังนี้
    - เปลี่ยนค่า `ddl-auto` จาก none เป็น update (ใช้สำหรับการทดลองเท่านั้น)
    - เปลี่ยนค่า `url` ของ `datasource` จาก database เป็นชื่อฐานข้อมูลที่เพิ่งสร้าง
    - เปลี่ยนค่า `username` และ `password` ของ `datasource` เป็น username และ password ของผู้ใช้ฐานข้อมูลที่เพิ่งสร้าง
    - เปลี่ยนค่า `username` และ `password` ของ`mail` เป็น username และ password(รหัสผ่านสำหรับแอป) ของอีเมลที่เพิ่งสร้าง
### โอสต์บนอีเมล
หากไม่ได้ทดลองบน localhost ให้เปลี่ยนข้อมูลในไฟล์ที่อยู่ในโฟลเดอร์ `src/main/resources/templates/mail/` คือ `mailUnregisterTemplate.html` และ `mailRegisterTemplate.html` 
โดยเปลี่ยนค่าจาก localhost:8080 เป็นชื่อโฮสต์ที่ต้องการ
### รัน
Linux หรือ MacOS:
```shell
./mvnw spring-boot:run
```
Windows:
```shell
mvnw spring-boot:run
```
หากไม่เกิดข้อผิดพลาด สามารถเข้าดูผลลัพธ์ที่ http://localhost:8080/
## การใช้งาน
### ผู้ใช้สมัครรับข้อมูล<br/>
1. กรอกที่อยู่อีเมล<br/>
![register input email](screenshots/register-input-email.png)<br/><br/>
2. กดปุ่ม **สมัคร**<br/>
![register input email after](screenshots/register-input-email-after.png)<br/><br/>
3. ยืนยันการสมัครผ่านอีเมลที่ได้รับ<br/>
<img src="screenshots/register-confirm-email.png" alt="register confirm email" width="600" /><br/>
<img src="screenshots/register-confirm-email-after.png" alt="register confirm email after " width="600" /><br/>
4. ได้รับอีเมลแสดงข้อมูลสถิติของโรคระบาดโควิด-19<br/>
<img src="screenshots/email-info.png" alt="email info" width="400" /><br/>

### ผู้ใช้ยกเลิกรับข้อมูล
1. เลือกเมนูยกเลิกรับข้อมูล<br/>
![unregister button](screenshots/unregister-button.png)<br/><br/>
2. กรอกที่อยู่อีเมล<br/>
![unregister input email](screenshots/unregister-input-email.png)<br/><br/>
3. กดปุ่ม **ยกเลิก**<br/>
![unregister input email after](screenshots/unregister-input-email-after.png)<br/><br/>
4. ยืนยันการยกเลิกผ่านอีเมลที่ได้รับ<br/>
<img src="screenshots/unregister-confirm-email.png" alt="unregister confirm email" width="600" /><br/>
<img src="screenshots/unregister-confirm-email-after.png" alt="unregister confirm email after" width="600" /><br/>

## ผู้จัดทำ
โปรเจคท์นี้เป็นส่วนหนึ่งของรายวิชาเว็บขั้นสูง โดยมีสมาชิกในทีมดังนี้
- นาย สิทธิเดช แก้วระวัง(หัวหน้าทีม) sidthidet.kaeoravang@gmail.com
- นาย ธีรภัทร ไทรแก้ว 622021121@tsu.ac.th
- นางสาว พาตรี บอเนาะ 622021114@tsu.ac.th
