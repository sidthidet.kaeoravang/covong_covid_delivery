package tsu.cs.advanced_web.covong_covid_delivery.business;

import java.util.Objects;
import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.UserModel;
import tsu.cs.advanced_web.covong_covid_delivery.model.UserRegisterModel;
import tsu.cs.advanced_web.covong_covid_delivery.repository.UserRepository;

@Service
public class RegisterBusiness {
 
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailRegisterBusiness mailRegister;

    public UserRegisterModel register(UserRegisterModel user_register) throws UserException, MessagingException {
        if (Objects.isNull(user_register)) {
            throw UserException.registerNull();
        }

        String user_register_email = user_register.getEmail();

        if (user_register_email.isEmpty()) {
            throw UserException.registerEmailNull();
        }

        if (userRepository.findByEmail(user_register_email).isPresent()) {   
            throw UserException.registerEmailDuplicated();
        }

        UserModel new_user = new UserModel();

        String verify_code = UUID.randomUUID().toString();
        new_user.setEmail(user_register_email);
        new_user.setVerifyCode(verify_code);
        new_user.setEnabled(false);

        mailRegister.prepareAndSend(user_register_email, mailRegister.registerBuildContent(verify_code));

        userRepository.save(new_user);

        UserRegisterModel user_register_respone = new UserRegisterModel();
        user_register_respone.setEmail(new_user.getEmail());
        return user_register_respone;
    }

    public UserRegisterModel unregister(UserRegisterModel user_unregister) throws UserException, MessagingException {
        if (Objects.isNull(user_unregister)) {
            throw UserException.unregisterNull();
        }

        String user_unregister_email = user_unregister.getEmail();

        if (user_unregister_email.isEmpty()) {
            throw UserException.unregisterEmailNull();
        }

        if (userRepository.findByEmail(user_unregister_email).isEmpty()) {   
            throw UserException.registerEmailNotFound();
        }

        UserModel user = userRepository.findByEmail(user_unregister_email).get();

        if (!user.isEnabled()) {
            throw UserException.unregisterEmailNotConfirm();
        }

        String verify_code = UUID.randomUUID().toString();
        user.setVerifyCode(verify_code);

        mailRegister.prepareAndSend(user_unregister_email, mailRegister.unregisterBuildContent(verify_code));

        userRepository.save(user);

        UserRegisterModel user_unregister_respone = new UserRegisterModel();
        user_unregister_respone.setEmail(user.getEmail());
        return user_unregister_respone;
    }

}
