package tsu.cs.advanced_web.covong_covid_delivery.business;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.CovidInformationModel;
import tsu.cs.advanced_web.covong_covid_delivery.repository.CovidInformationRepository;

@Service
public class MailCovidInformationUpdateBusiness {

    private TemplateEngine templateEngine;

    @Autowired
    public MailCovidInformationUpdateBusiness(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private CovidInformationRepository covidInformationRepository;
 
    public void prepareAndSend(String[] email_destination_list) throws MessagingException, UserException {
        String CovidInformationHtmlPage = this.buildContent(covidInformationRepository.findByLatestTxnDate());
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        
        helper.setTo("covong.covid.delivery@gmail.com");
        helper.setBcc(email_destination_list);
        helper.setSubject("Sent from Covong Covid Delivery");
        helper.setText(CovidInformationHtmlPage, true);

        try {
            javaMailSender.send(msg);
        } catch (Exception e) {
            throw UserException.registerEmailInvalid();
        }
    }

    public String buildContent(CovidInformationModel covid_information) {
        Context context = new Context();
        context.setVariable("covid_information", covid_information);
        return templateEngine.process("mail/mailCovidInformationTemplate", context);
    }
}
