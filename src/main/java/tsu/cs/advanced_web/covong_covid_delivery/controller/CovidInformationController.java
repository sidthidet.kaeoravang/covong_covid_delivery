package tsu.cs.advanced_web.covong_covid_delivery.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import tsu.cs.advanced_web.covong_covid_delivery.daemon.CovidInformationUpdateDaemon;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.CovidInformationModel;
import tsu.cs.advanced_web.covong_covid_delivery.repository.CovidInformationRepository;

@Controller()
public class CovidInformationController {

    @Autowired
    private CovidInformationRepository covidInformationRepository;

    @Autowired
    private CovidInformationUpdateDaemon covidInformationUpdateDaemon; 
    
    @GetMapping("/")
    public String getCovidInformation(Model model) throws MessagingException, UserException {
        if (covidInformationRepository.count() == 0) {
            covidInformationUpdateDaemon.UpdateChecker();
        }
        CovidInformationModel covid_information_latest = covidInformationRepository.findByLatestTxnDate();
        model.addAttribute("covid_information", covid_information_latest);
        return "index";
    }
}
