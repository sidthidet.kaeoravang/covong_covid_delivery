package tsu.cs.advanced_web.covong_covid_delivery.controller;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.Data;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;

@ControllerAdvice
public class ErrorAdviser {
    
    @ExceptionHandler(UserException.class)
    public ResponseEntity<ErrorResponse> handleBaseException(UserException e) {
        ErrorResponse response = new ErrorResponse();
        response.setError(e.getMessage());
        response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
        return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
    }

    @Data
    public static class ErrorResponse {

        private LocalDateTime timestamp = LocalDateTime.now();

        private int status;

        private String error;

    }
}
