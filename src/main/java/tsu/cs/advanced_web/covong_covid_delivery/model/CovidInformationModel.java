package tsu.cs.advanced_web.covong_covid_delivery.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity(name = "CovidInformation")
@Data
@JsonIgnoreProperties({"id", "new_case_excludeabroad", "total_case_excludeabroad", "update_date"})
public class CovidInformationModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    
    @Column(columnDefinition="date")
    @JsonProperty("txn_date")
    private String txnDate;

    @JsonProperty("new_case")
    private Integer newCase;

    @JsonProperty("total_case")
    private Integer totalCase;

    @JsonProperty("new_death")
    private Integer newDeath;

    @JsonProperty("total_death")
    private Integer totalDeath;

    @JsonProperty("new_recovered")
    private Integer newRecovered;

    @JsonProperty("total_recovered")
    private Integer totalRecovered;
}
