package tsu.cs.advanced_web.covong_covid_delivery.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity(name = "User")
@Data
public class UserModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    
    @NotNull
    private String email;

    @Column(length = 36)
    private String verifyCode;

    @NotNull
    private boolean enabled;
}
