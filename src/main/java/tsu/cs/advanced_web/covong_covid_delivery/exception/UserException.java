package tsu.cs.advanced_web.covong_covid_delivery.exception;

public class UserException extends Exception {

    public UserException(String code) {
        super("user." + code);
    }

    // Register
    public static UserException registerNull() {
        return new UserException("register.null");
    }

    public static UserException registerEmailNull() {
        return new UserException("register.email.null");
    }

    public static UserException registerEmailDuplicated() {
        return new UserException("register.email.duplicated");
    }

    public static UserException registerEmailInvalid() {
        return new UserException("register.email.invaild");
    }

    public static UserException registerConfirmInvalid() {
        return new UserException("register.confirm.invalid");
    }

    public static UserException registerConfirmNotFound() {
        return new UserException("register.confirm.fail");
    }

    // Unregister
    public static UserException unregisterNull() {
        return new UserException("unregister.null");
    }

    public static UserException unregisterEmailNull() {
        return new UserException("unregister.email.null");
    }

    public static UserException registerEmailNotFound() {
        return new UserException("unregister.email.not_found");
    }

    public static UserException unregisterEmailNotConfirm() {
        return new UserException("unregister.email.not_confirm");
    }

    public static UserException unregisterConfirmInvalid() {
        return new UserException("unregister.confirm.invalid");
    }

    public static UserException unregisterConfirmNotFound() {
        return new UserException("unregister.confirm.fail");
    }
}
