package tsu.cs.advanced_web.covong_covid_delivery.daemon;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import tsu.cs.advanced_web.covong_covid_delivery.business.MailCovidInformationUpdateBusiness;
import tsu.cs.advanced_web.covong_covid_delivery.exception.UserException;
import tsu.cs.advanced_web.covong_covid_delivery.model.CovidInformationModel;
import tsu.cs.advanced_web.covong_covid_delivery.repository.CovidInformationRepository;
import tsu.cs.advanced_web.covong_covid_delivery.repository.UserRepository;

@Component
public class CovidInformationUpdateDaemon {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	private CovidInformationRepository covidInformationRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Autowired
	MailCovidInformationUpdateBusiness mailCovidInformation;

	// @Scheduled(cron="*/5 * * * * *")
	@Scheduled(cron="0 0 * * * *")
	public void UpdateChecker() throws MessagingException, UserException {
        String api_url = "https://covid19.ddc.moph.go.th/api/Cases/today-cases-all";
        CovidInformationModel covid_information = restTemplate.getForEntity(api_url, CovidInformationModel[].class).getBody()[0];

		if (covidInformationRepository.findByTxnDate(covid_information.getTxnDate()).isEmpty()) {
			System.out.println("There is a new update");
			covidInformationRepository.save(covid_information);
			String[] all_activated_email_list = userRepository.getAllActivatedEmail();
			if (all_activated_email_list.length != 0) {
				mailCovidInformation.prepareAndSend(all_activated_email_list);
			}
		}
		System.out.println("The time is now " + dateFormat.format(new Date()));
	}
}
