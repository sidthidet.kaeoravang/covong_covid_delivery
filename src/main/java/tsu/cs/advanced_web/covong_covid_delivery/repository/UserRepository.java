package tsu.cs.advanced_web.covong_covid_delivery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tsu.cs.advanced_web.covong_covid_delivery.model.UserModel;

public interface UserRepository extends CrudRepository<UserModel, Integer>{

    Optional<UserModel> findByEmail(String email);

    Optional<UserModel> findByVerifyCode(String verifyCode);

    // @Query("SELECT email FROM user WHERE enabled = (1)")
    @Query(
        value = "SELECT email FROM user WHERE enabled = (1)",
        nativeQuery = true
    )
    public String[] getAllActivatedEmail();
}
