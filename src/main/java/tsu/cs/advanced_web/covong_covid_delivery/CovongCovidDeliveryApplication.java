package tsu.cs.advanced_web.covong_covid_delivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CovongCovidDeliveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovongCovidDeliveryApplication.class, args);
	}

}
